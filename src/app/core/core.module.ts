import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const MODULES = [];
const PROVIDERS = [];
const GUARDS = [];

@NgModule({
  imports: [
    CommonModule,
    ...MODULES,
  ],
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        ...PROVIDERS,
        ...GUARDS,
      ],
    };
  }
}
