import { ModuleWithProviders, NgModule } from '@angular/core';

@NgModule({
  exports: [],
  declarations: [],
})
export class DirectivesModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: DirectivesModule };
  }
}
