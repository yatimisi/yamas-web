import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule],
  exports: [
    CommonModule,
  ],
  declarations: [],
})
export class PipesModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: PipesModule };
  }
}
