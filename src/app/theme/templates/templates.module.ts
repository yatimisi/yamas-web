import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule],
  exports: [],
  declarations: [],
})
export class TemplatesModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: TemplatesModule };
  }
}
