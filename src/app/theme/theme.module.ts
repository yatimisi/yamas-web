import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from './components/components.module';
import { DirectivesModule } from './directives/directives.module';
import { LayoutsModule } from './layouts/layouts.module';
import { PipesModule } from './pipes/pipes.module';
import { TemplatesModule } from './templates/templates.module';

@NgModule({
  imports: [CommonModule],
  exports: [
    CommonModule,
    ComponentsModule,
    DirectivesModule,
    LayoutsModule,
    PipesModule,
    TemplatesModule,
  ],
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: ThemeModule };
  }
}
